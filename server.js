//Calling dependencies required here
var express=require('express')
var bodyParser=require('body-parser')
var mongoose =require('mongoose')


//Setting ref to a variable
var app=express()
var http=require('http').Server(app)
var io=require('socket.io')(http)


app.use(express.static(__dirname))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

//db Url with Auth
var dbUrl='mongodb://app_user:!QAZ2wsx@ds125482.mlab.com:25482/todo_xamdb'

//db Schema
var Message=mongoose.model('Message',{    
    _id:String,
    Title:String,
    Note:String,
    Done:Boolean
})


//GET
app.get('/messages',(req,res)=>{
    console.log("Get Called!")
    Message.find({},(err,messages)=>{
        res.send(messages)
    })    
})

//POST
app.post('/messages',(req,res)=>{
    var message=new Message(req.body)

    message.save((err)=>{
        if(err){            
            console.log("Error in Saving new Item")
            return res.sendStatus(500)
        }
               
        io.emit('message', req.body)
        res.sendStatus(200)
    })
})

//PUT
app.put('/messages',(req,res)=>{
    console.log("PUT Called!")
    var message=new Message(req.body)
    Message.findById(message._id,(err,upd)=>{
        if(err){
            res.sendStatus(404)
            console.log("Task not Found!")
        }
        upd.Title=message.Title;
        upd.Note=message.Note;
        upd.Done=message.Done;
upd.save((err1)=>{
    if(err1){
        console.log("Error in saving PUT")
        return res.sendStatus(500)    
    }    
    res.sendStatus(200)
    console.log("Updated by PUT")       
})
    })
})

//Delete
app.delete('/messages/:_id',(req,res)=>{
    var message=new Message(req.body)
    var a=req.originalUrl.split('/messages/')
    console.log(a[1])
    Message.remove({_id:a[1]},(err)=>{
        if(err){
            res.sendStatus(404)
            console.log("Task not Found!")
        }
        res.sendStatus(200)
        console.log("Deleted")
    })
    console.log("Delete Called : "+message._id)   
})


mongoose.connect(dbUrl,{useNewUrlParser:true},(err)=>{
    console.log('mongodb connection',err)
})

var server=http.listen(8000,()=>{
    console.log('Server is listening on port', server.address().port)
})